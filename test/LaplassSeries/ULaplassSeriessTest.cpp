#include <QtTest>
#include <boost/math/special_functions.hpp>
#include <ULaplassSeries.h>

using namespace boost::math;
class ULaplassSeriessTest : public QObject
{
    Q_OBJECT

public:
    ULaplassSeriessTest(){}

private Q_SLOTS:
    void caseWhenMEqualZero();
    void caseWhenMLargeTheL();
    void comparisonToSphericalFunctions();

};




void ULaplassSeriessTest::caseWhenMEqualZero()
{
    double phi = boost::math::constants::half_pi<double>()/2, theta = boost::math::constants::half_pi<double>()/2;


    auto modelCoff = std::function<void(uint,uint,ULaplassianSeriesCofficients<double>*) >(
                [](uint l,uint m,ULaplassianSeriesCofficients<double> * returninCoff)->void{ returninCoff->phase =0; returninCoff->amplitude = (l == 0 && m == 0) ? 0 : 1; });

    auto lapSerriesTest = [&](uint l, uint m){ return ULaplassianSeries<double>::calculate(l, m, phi, cos(theta), modelCoff);};
    double result = lapSerriesTest(1, 0);

    double comparison = spherical_harmonic_r<double>(1, 0, theta, phi);

    QCOMPARE(result, comparison);


    result = lapSerriesTest(3, 0);

    comparison = 0;
    for(int i=1; i !=4; ++i) comparison += spherical_harmonic_r<double>(i, 0, theta, phi);
    QCOMPARE(result, comparison);
}

void ULaplassSeriessTest::caseWhenMLargeTheL()
{
    double phi = boost::math::constants::half_pi<double>()/2, theta = boost::math::constants::half_pi<double>()/2;


    auto modelCoff = std::function<void(uint,uint,ULaplassianSeriesCofficients<double>*) >(
                [](uint l,uint m,ULaplassianSeriesCofficients<double> * returninCoff)->void{ returninCoff->phase =0; returninCoff->amplitude = (l == 0 && m == 0) ? 0 : 1; });
    auto lapSerriesTest = [&](uint l, uint m){ return ULaplassianSeries<double>::calculate(l, m, phi, cos(theta), modelCoff);};

    double m1 = lapSerriesTest(1,1);
    double m2 = lapSerriesTest(1,2);
    QCOMPARE( m1, m2);
}

void ULaplassSeriessTest::comparisonToSphericalFunctions()
{
    double phi = boost::math::constants::half_pi<double>()/2, theta = boost::math::constants::half_pi<double>()/2;


    auto modelCoff = std::function<void(uint,uint,ULaplassianSeriesCofficients<double>*) >(
                [](uint l,uint m,ULaplassianSeriesCofficients<double> * returninCoff)->void{ returninCoff->phase =0; returninCoff->amplitude = m+l !=0?(m+l):1 ; });

    auto lapSerriesTest = [&](uint l, uint m){ return ULaplassianSeries<double>::calculate(l, m, phi, cos(theta), modelCoff);};
    double result =  lapSerriesTest(2, 2);


    double comparison =0;

    for(int l=0; l !=3; ++l){
        for(int m =0; m <=l; ++m){
            comparison += (m+l !=0?(m+l):1)*spherical_harmonic_r<double>(l, m, theta, phi);
        }
    }
    QCOMPARE(result, comparison);
}

QTEST_APPLESS_MAIN(ULaplassSeriessTest)

#include "ULaplassSeriessTest.moc"
