#include <QtTest>
#include <ELPMPP02.h>

using namespace Ursa;
class ELPMPP02Test : public QObject
{
    Q_OBJECT

public:
    ELPMPP02Test();

private Q_SLOTS:
    void llrFitedInternal();
    void llrFitedOriginal();
    void de405FittedInternal();
    void de405FitedOriginal();
};



ELPMPP02Test::ELPMPP02Test()
{

}

void ELPMPP02Test::llrFitedInternal()
{
    Ursa::ELPMPP02 elpMPP02;
    SphericalCoordinate tmp;
    elpMPP02.calculate(2444239.5 - 2451545.0, &tmp);

    SphericalCoordinate comparison = {-1676.1543165348342, -0.082301011444206262, 385008.92666040978};

    QCOMPARE(tmp, comparison);
}

void ELPMPP02Test::llrFitedOriginal()
{

}

void ELPMPP02Test::de405FittedInternal()
{
    ELPMPP02 elpMPP02;
    SphericalCoordinate tmp;
    elpMPP02.setFitCoff(ELPMPP02::DE405);
    elpMPP02.calculate(2500000.5 - 2451545.0, &tmp);

    SphericalCoordinate comparison = {11147.114413267083,  -0.050757205196552843, 372818.92764608312};

    QCOMPARE(tmp, comparison);
}

void ELPMPP02Test::de405FitedOriginal()
{

}

QTEST_APPLESS_MAIN(ELPMPP02Test)

#include "ELPMPP02Test.moc"
