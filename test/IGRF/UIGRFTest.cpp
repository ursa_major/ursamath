#include <QtTest>
#include <boost/math/special_functions.hpp>
#include <UIGRF.h>
#include <UrsaGlobal.h>



using namespace boost::math;
class UIGRFTest : public QObject
{
    Q_OBJECT

public:
    UIGRFTest(){}

private Q_SLOTS:
    void ItemMEqualZero();

};


void UIGRFTest::ItemMEqualZero()
{

    UIGRF gemagneticFieldModel;
    UVector<double> result;
    double longitude = 45.0_arcdeg, lattitude = 45.0_arcdeg;
    gemagneticFieldModel.calculate(6371.2, lattitude, longitude, &result);

    double total = result.module();


    QCOMPARE(total, 0.0);

}

QTEST_APPLESS_MAIN(UIGRFTest)

#include "UIGRFTest.moc"
