#include <QtTest>

#include <UIGRF.h>

#include <ULaplassSeries.h>
#include <ULaplassSeriesDerivative.h>


#include <ursaglobal.h>

using namespace boost::math;
class ULaplassSeriesDerivativeTest : public QObject
{
    Q_OBJECT

public:
    ULaplassSeriesDerivativeTest(){}

private Q_SLOTS:
    void caseWhenYEqualZero();
    void caseSmallStep();

};


void ULaplassSeriesDerivativeTest::caseWhenYEqualZero()
{
/*
    const auto modelCoff =  std::function<void(uint,uint,UTrigonometricCofficients<double>*) >(
                [](uint l,uint m,UTrigonometricCofficients<double> * returninCoff)->void{ returninCoff->phase =0; returninCoff->amplitude = (l == 0 && m == 0) ? 0 : 1; });

    ULaplassSeriesDerivative<double> laplassDerivative(1,0);
    laplassDerivative.setModelCoffFunction(modelCoff);
    double dx, dy;

    laplassDerivative.calculate( 0, cos(45.0_arcdeg), &dy, &dx);

    QCOMPARE(dy, 0.0);
    QCOMPARE(dx, 1.0/sqrt(4*boost::math::constants::pi<double>()));
    */
}

void ULaplassSeriesDerivativeTest::caseSmallStep()
{
    auto modelCoff =  std::function<void(uint,uint,ULaplassianSeriesCofficients<double>*) >(
                [](uint l,uint m,ULaplassianSeriesCofficients<double> * returninCoff)->void{ returninCoff->phase =0; returninCoff->amplitude = (l+m); });

    uint l =3, m =3;

    auto modelCoffDerivative =  std::function<void(uint,uint,ULaplassianSeriesCofficients<double>*) >(
                [&](uint l,uint m,ULaplassianSeriesCofficients<double> * returninCoff)->void{ modelCoff(l,m,returninCoff);
                returninCoff->amplitude *=sqrt((l+0.5)*factorial<double>(l-m)/factorial<double>(l+m) ) ;
                });
    ULaplassSeriesDerivative<double> laplassDerivative(l,m);
    laplassDerivative.setModelCoffFunction(modelCoffDerivative);
    double dx, dy;

    double argument = 0.5;
    laplassDerivative.calculate( argument, argument, &dy, &dx);

    double dArgument = 1E-9;
    double dxdt = (ULaplassianSeries<double>::calculate(l, m, argument, argument + dArgument, modelCoff)
                  -ULaplassianSeries<double>::calculate(l, m, argument, argument, modelCoff) )/dArgument;
    double dydt = (ULaplassianSeries<double>::calculate(l, m, argument + dArgument, argument , modelCoff)
                  -ULaplassianSeries<double>::calculate(l, m, argument, argument, modelCoff) )/dArgument;

    QVERIFY( fabs(dx - dxdt) <1E-5);
    QVERIFY(fabs(dy - dydt) <1E-5);
}

QTEST_APPLESS_MAIN(ULaplassSeriesDerivativeTest)

#include "ULaplassSeriesDerivativeTest.moc"

