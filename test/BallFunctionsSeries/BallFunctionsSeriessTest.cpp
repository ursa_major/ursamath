#include <QtTest>
#include <boost/math/special_functions.hpp>
#include <BallFunctionsSeriess.h>
#include <LaplassSeries.h>
#include <Vector.h>
#include <UrsaGlobal.h>

#include <boost/math/special_functions/gamma.hpp>

using namespace boost::math;
using namespace Ursa;
class BallFunctionsSeriessTest : public QObject
{
    Q_OBJECT

public:
    BallFunctionsSeriessTest(){}

private Q_SLOTS:
    void checkDifThetaAndPhi();
    void compareWithNumericDifferntial();

};


void BallFunctionsSeriessTest::checkDifThetaAndPhi()
{
    BallFunctionsSeriess<double> seriess;
    Vector<double> result, comparison;
    const auto modelCoff =  std::function<void(uint,uint,TrigonometricCofficients<double>*) >(
                [](uint l,uint m,TrigonometricCofficients<double> * returninCoff)->void{ returninCoff->phase =0; returninCoff->amplitude = 1; });
    seriess.setModelCoof(modelCoff);
    seriess.setOrders(3,3);
    seriess.derivative( 1, 45.0_arcdeg, 45.0_arcdeg, &(result[0]),&(result[1]),&(result[2]));

    LaplassSeriesDerivative<double>  laplassDeriative;
    laplassDeriative.setOrders(3,3);
    laplassDeriative.setModelCoffFunction(modelCoff);
    laplassDeriative.calculate( 45.0_arcdeg, cos(45.0_arcdeg), &(comparison[0]),&(comparison[1]));

    comparison[1] *= -sin(45.0_arcdeg);
    QCOMPARE(result(1), comparison(0));
    QCOMPARE(result(2), comparison(1));
}

void BallFunctionsSeriessTest::compareWithNumericDifferntial()
{
    double r=0.5, x=45.0_arcdeg, y = 15.0_arcdeg;
    uint l =3, m =3;
    const auto modelCoff = std::function<void(uint,uint,TrigonometricCofficients<double>*) >(
                [&](uint l,uint m,TrigonometricCofficients<double> * returninCoff)->void{ returninCoff->phase =0; returninCoff->amplitude = l!=0 ? 1:0;
                returninCoff->amplitude *=sqrt((l+0.5)*factorial<double>(l-m)/factorial<double>(l+m) ) ;
                });

    BallFunctionsSeriess<double> seriess;
    Vector<double> result;

    seriess.setModelCoof(modelCoff);

    seriess.setOrders(l,m);
    seriess.derivative( r, y, x, &(result[0]),&(result[1]),&(result[2]));

    auto modelCoffLaplaa =    std::function<void(uint,uint,TrigonometricCofficients<double>*) >(
                [&](uint l,uint m,TrigonometricCofficients<double> * returninCoff)->void{ returninCoff->phase =0;
                    returninCoff->amplitude = l!=0 ? pow(r, l+1):0;

                    });

    double darg =1E-9;

    double dx = (LaplassianSeries<double>::calculate(l,m, y, cos(x+darg), modelCoffLaplaa) -  LaplassianSeries<double>::calculate(l,m, y, cos(x), modelCoffLaplaa))/darg;
    double dy = (LaplassianSeries<double>::calculate(l,m, y+darg, cos(x), modelCoffLaplaa) -  LaplassianSeries<double>::calculate(l,m, y, cos(x), modelCoffLaplaa))/darg;
    double dr =  LaplassianSeries<double>::calculate(l,m, y, cos(x), modelCoffLaplaa);
    r +=darg;
    dr -= LaplassianSeries<double>::calculate(l,m, y, cos(x), modelCoffLaplaa);
    dr /=-darg;
    QVERIFY(fabs(result(0) - dr) <1E-4);
    QVERIFY(fabs(result(1) - dy) <1E-4);
    QVERIFY(fabs(result(2) - dx) <1E-4);
}

QTEST_APPLESS_MAIN(BallFunctionsSeriessTest)

#include "BallFunctionsSeriessTest.moc"
