#pragma once
#include <vector>
#include <functional>

#include <boost/math/special_functions/legendre.hpp>
#include <boost/math/special_functions/factorials.hpp>

#include <Ursaglobal.h>

#include <Vector.h>
#include <TrigonometricCofficients.h>

namespace Ursa {


/**
 *\brief Calculate partial derivative of a function determine by LaplasSeries
 */

template <typename T> class LaplassSeriesDerivative
{
protected:
    static constexpr T Divsqrt2pi = 1.0/sqrt(2*boost::math::constants::pi<T>());
public:

    LaplassSeriesDerivative(){}
    LaplassSeriesDerivative(uint l, uint m){setOrders(l,m);}

    void setOrders(uint l, uint m)
    {
        legendrePolynoms[0].resize(l+2);
            legendrePolynoms[1].resize(l+2);
        m_m = m;
        m_l = l;
        border = std::min(m, l) + 1;
    }
    inline uint mOrder(){return m_m;}
    inline uint lOrder(){return m_l;}

    inline void setModelCoffFunction(const std::function<void(uint, uint, TrigonometricCofficients<T>*)> & model) {m_modelCoff = model;}
    inline const std::function<void(uint, uint, TrigonometricCofficients<T>)> & modelCoffFunction()const{return m_modelCoff;}

    void calculate(T y, T x, T * dy, T * dx) const
    {
        T dxStep, dYstep;
        T startCoff =1;
        this->x = x;
            this->y = y;
        DivXCoff1 =  - x/(1-x*x);
            DivXCoff2 = -1.0/sqrt(1-x*x);

        *dy =0, *dx=0;
        firstStep();

        for(uint mCounter =0; mCounter != border; ++mCounter)
        {
            uint calculatedMCounter= mCounter+1;

            startCoff = boost::math::double_factorial<T>(2 * calculatedMCounter - 1);
            legendrePolynoms[1][calculatedMCounter - 1] = 0;
                legendrePolynoms[1][calculatedMCounter] = startCoff*pow(1-x*x, calculatedMCounter*0.5);
            if(calculatedMCounter&1)
                legendrePolynoms[1][mCounter+1] *= -1;

            calculateDerivative(mCounter, mCounter, &dYstep, &dxStep);
                *dx += dxStep; *dy += dYstep;
            for( uint lCounter = mCounter+1, lCalculated=calculatedMCounter+1; lCounter < m_l; ++lCounter, ++lCalculated){
                legendrePolynoms[1][lCalculated] = boost::math::legendre_next(lCalculated-1, calculatedMCounter, x, legendrePolynoms[1][lCalculated-1], legendrePolynoms[1][lCalculated-2]);
                calculateDerivative(lCounter, mCounter, &dYstep, &dxStep);
                *dx += dxStep; *dy += dYstep;
            }
            if(m_l != mCounter){
                calculateDerivative(m_l, mCounter, &dYstep, &dxStep);
                *dx += dxStep; *dy += dYstep;
            }

            legendrePolynoms[0].swap(legendrePolynoms[1]);
        }
        *dy *= Divsqrt2pi; *dx *= Divsqrt2pi;
    }
protected:
    virtual void  calculateDerivative(uint lCounter, uint mCounter, T *dy, T * dx) const
    {
        T mCounterMulY = mCounter*y;

        m_modelCoff(lCounter, mCounter, &modelCoffValue);
        T dxPart =  DivXCoff2*legendrePolynoms[1][lCounter];
        dxPart += DivXCoff1*legendrePolynoms[0][lCounter]*mCounter;
        *dx = modelCoffValue.amplitude*cos(mCounterMulY - modelCoffValue.phase)*dxPart;
            *dy = -1.0*modelCoffValue.amplitude*sin(mCounterMulY - modelCoffValue.phase)*mCounter*legendrePolynoms[0][lCounter];
    }

    void firstStep() const
    {
        legendrePolynoms[0][0] = 1;
        if(m_l >= 1) legendrePolynoms[0][1] =  boost::math::legendre_next<T>(0, x, legendrePolynoms[0][0], (T)(0) );
        for(uint i = 2; i <= m_l; ++i ) legendrePolynoms[0][i] = boost::math::legendre_next(i-1, x, legendrePolynoms[0][i-1], legendrePolynoms[0][i-2]);
    }

protected:
    mutable std::vector<T> legendrePolynoms[2];
    mutable T x, y;
    mutable T DivXCoff1, DivXCoff2;
    mutable TrigonometricCofficients<T> modelCoffValue;

    uint border;
    uint m_l, m_m;
    mutable std::function<void(uint, uint, TrigonometricCofficients<T>*)> m_modelCoff;
};

}
