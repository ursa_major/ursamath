#pragma once
#include <Ursaglobal.h>

namespace Ursa {
    template <typename T> struct TrigonometricCofficients
    {
        T amplitude;
        T phase;
    };
}
