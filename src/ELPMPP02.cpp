#include "ELPMPP02.h"
#include "ELPMPP02_p.h"
#include "./ELP200ModifyCofficients/ELP_MAIN.S1.h"
#include "./ELP200ModifyCofficients/ELP_MAIN.S2.h"
#include "./ELP200ModifyCofficients/ELP_MAIN.S3.h"

#include "./ELP200ModifyCofficients/ELP_PERT.S1.h"
#include "./ELP200ModifyCofficients/ELP_PERT.S2.h"
#include "./ELP200ModifyCofficients/ELP_PERT.S3.h"


namespace Ursa {


using namespace Elp200ModifyCofficients;

extern constexpr ELPMPP02Private::FitCofficients ELPMPP02Private:: fitCoff[2];
ELPMPP02Private::ELPMPP02Private()
{

    m_correctedCoffNumber = 0;
    m_fitCoff = fitCoff;
}


ELPMPP02::ELPMPP02(UseFitCoff fitcoff): d(new ELPMPP02Private())
{
    setFitCoff(fitcoff);
}

ELPMPP02::~ELPMPP02()
{
    delete d;
}

/**
 * calculate moon position in ecliptical coordinate at current epoch
 */

void ELPMPP02::calculate(double mjdTDB, SphericalCoordinate *vector) const
{
    d->calculate(mjdTDB, vector);
}

/**
 * return fitCoffieint
 */
ELPMPP02::UseFitCoff ELPMPP02::fitCoff() const
{
    return static_cast<ELPMPP02::UseFitCoff>(d->m_correctedCoffNumber);
}

/**
 * set fitCoffieint
 */

void ELPMPP02::setFitCoff(UseFitCoff value)
{
    d->m_correctedCoffNumber = value;
    d->correctAmplitudeCofficients();
}

void ELPMPP02Private::calculate(double mjdTDB, SphericalCoordinate *vector) const
{
    double t[5];
    static constexpr double a405=384747.9613701725;
    static constexpr double aelp=384747.980674318;

    t[0]=1.0;
        t[1]=mjdTDB/36525;
        t[2]=t[1]*t[1];
        t[3]=t[2]*t[1];
        t[4]=t[3]*t[1];

    FundamentalArguments fundamental;
        calculateFundamentalArguments(t, &fundamental);
    TotalModelArguments modelArguments;
        calculateArguments(t, &fundamental, &modelArguments);

    double longitude = mainProblem(modelArguments.deloneArguments, longitudeMainSequenceCofficints, &sin) + perturbation(modelArguments, longitudePerturbationsSequenceCofficints, t);
    double lattitude = mainProblem(modelArguments.deloneArguments, lattitudeMainSequenceCofficints, &sin) + perturbation(modelArguments, lattitudePerturbationsSequenceCofficints, t);
    double distance = mainProblem(modelArguments.deloneArguments, radiusMainSequenceCofficints, &cos) + perturbation(modelArguments, radiusPerturbationsSequenceCofficints, t);


    vector->longitude = longitude*1.0_arcsec + fundamental.meanMeanLongitudeOfTheMoon;
    vector->lattitude = lattitude *1.0_arcsec;
    vector->radius = distance * a405/aelp;
}

double ELPMPP02Private::mainProblem(const  ELPMPP02Private::DeloneArguments &arguments,
                                         const vector< ELPMPP02Private::MainSequenceStruct> &deloneCofficients,  double (*ptrfun)(double)) const
{
    double furieArgument;
    auto argumentsPtr = (const double *) &arguments;

    double value =0;
    for( auto iter = deloneCofficients.begin(); iter != deloneCofficients.end(); ++iter )
    {
        furieArgument =0;
        for(int i =0; i!=4; ++i) furieArgument+= argumentsPtr[i]*(iter->deloneCofficients[i]);
        value += iter->correctedMainCofficient*ptrfun(furieArgument);
    }
    return value;
}

double ELPMPP02Private::perturbation(const  ELPMPP02Private::TotalModelArguments &arguments,
                                          const vector<vector< ELPMPP02Private::PerturbationSequenceStruct> > &cofficeints, const double *t) const
{
    double v[4] = {0,0,0,0};
    double phi=0;
    auto argumentsPtr = reinterpret_cast<const double*>(&arguments);
    double temp;
    const int cofficientsNumber = 13;
    for(uint i =0; i !=cofficeints.size(); ++i)
    {
        temp=0;
        auto& currentSequence =  cofficeints[i];
        for(uint sequenceNumber =0; sequenceNumber != cofficeints[i].size(); ++sequenceNumber){
            auto& currentCofficient = currentSequence[sequenceNumber];
            phi = currentCofficient.scPhi;
            for(int coffindex =0; coffindex != cofficientsNumber; ++coffindex){
                phi += currentCofficient.phiCofficients[coffindex]*argumentsPtr[coffindex];
            }
            temp += currentCofficient.scModule*sin(phi);
        }
        v[i] = temp;
    }
    double result=0;
    for(int i =0; i !=4 ;++i) result += v[i]*t[i];
    return result;
}

void ELPMPP02Private::calculateFundamentalArguments(const double *t, ELPMPP02Private::FundamentalArguments *args) const
{
    args->meanMeanLongitudeOfTheMoon = DMStoRad(218,18,59.95571)+m_fitCoff->Dw1_0 + (1732559343.73604_arcsec +m_fitCoff->Dw1_1)*t[1]+
                                      (-6.8084_arcsec + m_fitCoff->Dw1_2)*t[2] +  + 0.0066040_arcsec*t[3] - 0.31690E-4_arcsec*t[4];

    args->meanLongitudeLunarperigei = DMStoRad(83,21,11.67475) + m_fitCoff->Dw2_0 + (14643420.3171_arcsec +m_fitCoff->Dw2_1)*t[1]
                                      -38.2631_arcsec*t[2] - 0.45047e-1_arcsec*t[3] + 0.21301e-3_arcsec*t[4];
    args->meanLunarAscendingNode = DMStoRad(125, 2,40.39816) + m_fitCoff->Dw3_0 + (-6967919.5383_arcsec +m_fitCoff->Dw3_1)*t[1]
                                   +6.3590_arcsec*t[2] + 0.76250e-2_arcsec*t[3] - 0.35860e-4_arcsec*t[4];

    args->heleocentricLongitudeEMB = DMStoRad(100,27,59.13885)+m_fitCoff->Deart_0 + (129597742.29300_arcsec +m_fitCoff->Deart_1)*t[1]
                                        -0.020200_arcsec*t[2] + 0.90000e-5_arcsec*t[3] + 0.15000e-6_arcsec*t[4];

    args->heleocentricLongitudePerehelionEMB = DMStoRad(102,56,14.45766)+ m_fitCoff->Dperi + 1161.24342_arcsec*t[1]
                                                +0.529265_arcsec *t[2] - 0.11814e-3_arcsec*t[3] + 0.11379e-4_arcsec;
}

void ELPMPP02Private::calculateArguments(const double *t, const  ELPMPP02Private::FundamentalArguments *fundamental, ELPMPP02Private::TotalModelArguments *args) const
{
    DeloneArguments &deloneArguments = args->deloneArguments;
    deloneArguments.D = fundamental->meanMeanLongitudeOfTheMoon - fundamental->heleocentricLongitudeEMB + M_PI;
        deloneArguments.F = fundamental->meanMeanLongitudeOfTheMoon - fundamental->meanLunarAscendingNode;
        deloneArguments.l = fundamental->meanMeanLongitudeOfTheMoon - fundamental->meanLongitudeLunarperigei;
        deloneArguments.lStroke = fundamental->heleocentricLongitudeEMB - fundamental->heleocentricLongitudePerehelionEMB;

    args->eta = fundamental->meanMeanLongitudeOfTheMoon +(5029.0966_arcsec - 0.29965_arcsec)*t[1];

    args->Mercury = DMStoRad(252,15, 3.216919)  + 538101628.66888_arcsec*t[1];
    args->Venus = DMStoRad(181,58,44.7584190)   + 210664136.45777_arcsec*t[1];
    args->Terrestial = DMStoRad(100,27,59.138850)+129597742.29300_arcsec*t[1] ;
    args->Mars =  DMStoRad(355,26, 3.642778)    + 68905077.65936_arcsec*t[1];
    args->Jupiter = DMStoRad(34,21, 5.379392)   + 10925660.57335_arcsec*t[1];
    args->Saturn = DMStoRad(50, 4,38.902495)    + 4399609.33632_arcsec*t[1];
    args->Uran = DMStoRad(314, 3, 4.354234)     + 1542482.57845_arcsec*t[1];
    args->Neptune =DMStoRad(304,20,56.808371)   + 786547.89700_arcsec*t[1];
}

void  ELPMPP02Private::correctAmplitudeCofficients()
{
    static constexpr double am     =  0.074801329;
    static constexpr double alpha  =  0.002571881;
    static constexpr double dtasm  =  (2.0*alpha)/(3.0*am);
    m_fitCoff =  fitCoff + m_correctedCoffNumber;

    double delnu  = (+0.55604_arcsec + m_fitCoff->Dw1_1)/(1732559343.73604_arcsec + m_fitCoff->Dw1_1);
    double dele   = +0.01789_arcsec + m_fitCoff->De;
    double delg   = -0.08066_arcsec + m_fitCoff->Dgam;
    double delnp  = (-0.06424_arcsec + m_fitCoff->Deart_1)/(1732559343.73604_arcsec + m_fitCoff->Dw1_1);
    double delep  = -0.12879_arcsec + m_fitCoff->Dep;

    auto  correctsumm = [&](const MainSequenceStruct &correctcoff){
        const double *ptr = correctcoff.correctionsCofficients;
        return (ptr[0] + dtasm*ptr[4])*(delnp-am*delnu) + ptr[1]*delg + ptr[2]*dele +ptr[3]*delep;
    };

    for(auto iter = longitudeMainSequenceCofficints.begin(); iter != longitudeMainSequenceCofficints.end(); ++iter){
        iter->correctedMainCofficient = iter->mainACofficient +correctsumm(*iter);
    }
    for(auto iter = lattitudeMainSequenceCofficints.begin(); iter != lattitudeMainSequenceCofficints.end(); ++iter){
        iter->correctedMainCofficient = iter->mainACofficient +correctsumm(*iter);
    }
    for(auto iter = radiusMainSequenceCofficints.begin(); iter != radiusMainSequenceCofficints.end(); ++iter){
        iter->correctedMainCofficient = iter->mainACofficient + correctsumm(*iter) - 2.0*iter->mainACofficient*delnu/3.0;
    }
}
}
