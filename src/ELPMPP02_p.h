#pragma once

#include <Ursaglobal.h>
#include <cmath>
#include <vector>

namespace Ursa {

using namespace std;

class ELPMPP02Private{
public:
    ELPMPP02Private();
    struct MainSequenceStruct{
        int deloneCofficients[4];
        double mainACofficient;
        double correctionsCofficients[6];
        double correctedMainCofficient;
    };

    struct PerturbationSequenceStruct{
        double scPhi;
        double scModule;
        int phiCofficients[13];
    };
    struct FundamentalArguments{
        double meanMeanLongitudeOfTheMoon;
        double meanLongitudeLunarperigei;
        double meanLunarAscendingNode;
        double heleocentricLongitudeEMB;
        double heleocentricLongitudePerehelionEMB;
    };

    struct DeloneArguments{
        double D;
        double F;
        double l;
        double lStroke;
    };

    struct TotalModelArguments{
        DeloneArguments deloneArguments;
        double Mercury;
        double Venus;
        double Terrestial;
        double Mars;
        double Jupiter;
        double Saturn;
        double Uran;
        double Neptune;
        double eta;
    };

    struct FitCofficients {
        double Dw1_0;
        double Dw2_0;
        double Dw3_0;
        double Deart_0;
        double Dperi;
        double Dw1_1;
        double Dgam;
        double De;
        double Deart_1;
        double Dep;
        double Dw2_1;
        double Dw3_1;
        double Dw1_2;
    };
    static constexpr FitCofficients fitCoff[2] = {
    {-0.10525_arcsec, 0.16826_arcsec,-0.10760_arcsec, -0.04012_arcsec, -0.04854_arcsec, -0.32311_arcsec, 0.00069_arcsec,
     0.00005_arcsec, 0.01442_arcsec, 0.00226_arcsec, 0.08017_arcsec, -0.04317_arcsec, -0.03794_arcsec},
    {-0.07008_arcsec, 0.20794_arcsec, -0.07215_arcsec, -0.00033_arcsec, -0.00749_arcsec, -0.35106_arcsec, 0.00085_arcsec,
     -0.00006_arcsec, 0.00732_arcsec, 0.00224_arcsec, 0.08017_arcsec, -0.04317_arcsec, -0.03743_arcsec}
    };

    double mainProblem(const DeloneArguments & arguments, const vector<MainSequenceStruct> & deloneCofficients,  double (*ptrfun)(double)) const;
    double perturbation(const TotalModelArguments &arguments, const vector<vector<PerturbationSequenceStruct> > &cofficeints , const double *v) const;
    void calculateFundamentalArguments(const double *t, FundamentalArguments *args) const;
    void calculateArguments(const double *T, const FundamentalArguments *fundamental, TotalModelArguments *args) const;
    void correctAmplitudeCofficients();
    void calculate(double jdTDB, SphericalCoordinate *vector) const;

    int m_correctedCoffNumber;
private:
    const FitCofficients *m_fitCoff;

};

}

