#pragma once
#include <Ursaglobal.h>
#include <Vector.h>

namespace Ursa {
    class IGRFPrivate;
    class URSA_EXPORT IGRF
    {
    public:
        IGRF();
        ~IGRF();

        void calculate(double radiusKM, double lattitude, double longitude, Vector<double> *result);
    private:
        IGRFPrivate *d;
    };
}
