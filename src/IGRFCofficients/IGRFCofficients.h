#pragma once
#include <Ursaglobal.h>
#include <LaplassSeries.h>
#include <math.h>
namespace Ursa {


namespace IGRFCofficients {
    struct IGRFCofficients: public LaplassianSeriesCofficients<double>{
        IGRFCofficients(double x, double y)
        {
            amplitude = sqrt(x*x + y*y);
            phase =  x !=0 || y!=0? atan2(y, x): 0;
        }
    };
}
}
