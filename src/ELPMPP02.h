#pragma once
#include <Ursaglobal.h>

namespace Ursa {
/**
 *\brief Implementation Moon's ephemerics model - ELPMPP02.
 *
 * You can learn more at ftp://cyrano-se.obspm.fr/pub/2_lunar_solutions/2_elpmpp02/
 */

/**
 *\property fitCoff
 *
 */
    class ELPMPP02Private;
    class URSA_EXPORT  ELPMPP02
    {
    public:
        enum UseFitCoff{LLR=0, ///< Correspond to LLR Measurements
                        DE405=1 ///< Correspond to DE405 ephemerics
                       };
    public:
        ELPMPP02(UseFitCoff fitcoff = LLR);
        ~ELPMPP02();
        void calculate(double mjdTDB, SphericalCoordinate *vector) const;
        UseFitCoff fitCoff() const;
        void setFitCoff(UseFitCoff value);
    private:
        ELPMPP02Private *d;
    };
}
