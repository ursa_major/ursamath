import qbs

Ursacommonlibraryproject{
    name:"Math"
    files: [
        "BallFunctionsSeriess.h",
        "ELP200ModifyCofficients/ELP_MAIN.S1.h",
        "ELP200ModifyCofficients/ELP_MAIN.S2.h",
        "ELP200ModifyCofficients/ELP_MAIN.S3.h",
        "ELP200ModifyCofficients/ELP_PERT.S1.h",
        "ELP200ModifyCofficients/ELP_PERT.S2.h",
        "ELP200ModifyCofficients/ELP_PERT.S3.h",
        "ELPMPP02.cpp",
        "ELPMPP02.h",
        "ELPMPP02_p.h",
        "IGRF.cpp",
        "IGRF.h",
        "IGRFCofficients/IGRF2015.h",
        "IGRFCofficients/IGRFCofficients.h",
        "LaplassSeries.h",
        "LaplassSeriesDerivative.h",
        "TrigonometricCofficients.h",
    ]

    Depends{name:"Qt.core"}
    Depends{name:"UrsaDevelop.Core"}
    cpp.includePaths: ["./", EigenPath, BoostPath]

    srcPathFiles:path
    Export{
            Depends{name:"UrsaDevelop.Core"}
            Depends { name: "cpp" }
            Depends{name:"Qt.core"}
            cpp.includePaths: ["./", EigenPath, BoostPath]
    }
}
