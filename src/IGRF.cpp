#include "IGRF.h"
#include <functional>

#include <boost/math/special_functions/factorials.hpp>

#include <BallFunctionsSeriess.h>
#include <IGRFCofficients/IGRF2015.h>
#include <Vector.h>

namespace Ursa {

class IGRFPrivate
{
public:
    IGRFPrivate() {}
    BallFunctionsSeriess<double> ballPolynoms;
    static constexpr double a = 6371.2;
};

IGRF::IGRF(): d(new IGRFPrivate)
{
    auto modelcoff = std::function<void(uint,uint,TrigonometricCofficients<double>*)>(
                [&](uint l,uint m,LaplassianSeriesCofficients<double> * returninCoff)->void{
                    if (l == 0 && m == 0){
                            returninCoff->phase =0; returninCoff->amplitude =0;
                            return;
                    }
                    int number = (l+1)*l/2 -1 + m;
                    returninCoff->amplitude = IGRFCofficients::IGRF2015[number].amplitude;
                    if(m >0){
                        returninCoff->amplitude *= m&1? -1:1;
                        returninCoff->amplitude *=sqrt(2*factorial<double>(l-m)/factorial<double>(l+m));
                    }
                    returninCoff->phase = IGRFCofficients::IGRF2015[number].phase;

                  });

    d->ballPolynoms.setOrders(13,13);
    d->ballPolynoms.setModelCoof(modelcoff);
}

IGRF::~IGRF()
{
    delete d;
}

void IGRF::calculate( double radiusKM, double lattitude, double longitude, Vector<double>* result)
{
    double polarLattitude = boost::math::constants::half_pi<double>() - lattitude;
    double dlongitude, dradius, dLattittude;

    double rho = d->a/radiusKM;
    d->ballPolynoms.derivative(rho, longitude, polarLattitude, &dradius, &dlongitude, &dLattittude);

    dlongitude *= -rho;
        dLattittude *= rho; //without "-" because in dAlttidtude current value is dirivative by d(pi/2-longitude)
        dradius *= rho*rho;//without because in dradius current value is dirivative by d(a/radius)
    double root_pi2 = boost::math::constants::root_pi<double>()*boost::math::constants::root_two<double>();
    (*result)(0) = dradius*root_pi2;
    (*result)(1) = dLattittude*root_pi2;
    (*result)(2) = dlongitude/sin(polarLattitude)*root_pi2;

}

}
