#pragma once
#include <Ursaglobal.h>
#include <vector>
#include <boost/math/special_functions/legendre.hpp>
#include <functional>
#include <TrigonometricCofficients.h>

namespace Ursa {


using namespace boost::math;

template <typename T> using LaplassianSeriesCofficients = TrigonometricCofficients<T>;

template <typename T> class LaplassianSeries
{

public:
    template<typename CoffType> static  T calculate (uint l, uint m,
                                                     T y, T x, std::function<void(uint,uint,LaplassianSeriesCofficients<CoffType>*) >& modelCofficients )
    {
        T legendre[3];
        T startCoff =1, summ=0, totalSumm=0, normalCoff =1;

        uint border = std::min(m, l) + 1;
        LaplassianSeriesCofficients<CoffType> coff;

        for( uint mCounter =0; mCounter != border; ++mCounter)
        {
            int isMequalZero = mCounter == 0;
                legendre[0] = isMequalZero;

            startCoff = !isMequalZero? double_factorial<T>(2 * mCounter - 1): 1 ;

            legendre[1] = startCoff*pow(1-x*x, mCounter/2.0);
            normalCoff = 1.0/factorial<T>(2*mCounter);
            if(mCounter&1)
               legendre[1] *= -1;
            modelCofficients(mCounter, mCounter, &coff);
            summ = coff.amplitude*cos(mCounter*y - coff.phase)*legendre[1]*sqrt((2*mCounter+1)*normalCoff);
            for( uint lCounter = mCounter+1; lCounter <= l; ++lCounter){
                legendre[2] = legendre_next(lCounter-1, mCounter, x, legendre[1], legendre[0]);
                    normalCoff *= (lCounter - mCounter)/((T)lCounter + mCounter);
                modelCofficients(lCounter, mCounter, &coff);
                summ += coff.amplitude*cos(mCounter*y - coff.phase)*legendre[2]*sqrt((2*lCounter+1)*normalCoff);
                legendre[0] = legendre[1];
                    legendre[1] = legendre[2];
            }
            totalSumm +=summ;
        }
        return totalSumm/sqrt(4*constants::pi<T>());
    }

};

}
