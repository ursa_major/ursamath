#pragma once
#include <functional>
#include <vector>

#include <UrsaGlobal.h>
#include <LaplassSeriesDerivative.h>

namespace Ursa {

template <typename T> class BallFunctionsSeriess;
template <typename T> class BallTemplateFunctionsPrivate: protected LaplassSeriesDerivative<T>
{
    friend class BallFunctionsSeriess<T>;
private:
    using LaplassSeriesDerivative<T>::setModelCoffFunction;
    void setOrders(uint l, uint m)
    {
        LaplassSeriesDerivative<T>::setOrders(l, m);
        radiusCofficients.resize(l+1);
    }

    void calculateDerivative(uint lCounter, uint mCounter, T *dy, T * dx) const override
    {
        LaplassSeriesDerivative<T>::calculateDerivative(lCounter, mCounter, dy, dx);

        m_dRadius += this->modelCoffValue.amplitude*cos(mCounter*this->y - this->modelCoffValue.phase)*(lCounter+1)*
                radiusCofficients[lCounter]*this->legendrePolynoms[0][lCounter];
        *dy *= radiusCofficients[lCounter];
            *dx *= radiusCofficients[lCounter];
    }

    void calculate(T radius, T lambda, T theta, T *dradius, T *dlambda, T * dtheta)
    {
        m_R = radius;
        radiusCofficients[0] = radius;
        for(uint i =1; i < radiusCofficients.size(); ++i) radiusCofficients[i] = radiusCofficients[i-1]*radius;
        m_dRadius = 0;

        LaplassSeriesDerivative<T>::calculate(lambda, cos(theta), dlambda, dtheta);
        *dtheta *= -sin(theta);
        *dradius = m_dRadius/m_R * this->Divsqrt2pi;
    }
    mutable std::vector<T> radiusCofficients;
    mutable T m_dRadius, m_R;
};

template <typename T> class BallFunctionsSeriess
{
public:
    inline void setOrders(uint l, uint m){d.setOrders(l, m);}
    inline T radiusNormCofficient(){return d.radiusNormCofficient();}

    inline void setModelCoof ( std::function<void(uint, uint, TrigonometricCofficients<T>*)> value ){d.setModelCoffFunction(value);}

    void derivative(T radius, T lambda, T theta, T *dradius, T *dlambda, T * dtheta)
    {
        d.calculate(radius, lambda, theta, dradius, dlambda, dtheta);
    }

private:
    BallTemplateFunctionsPrivate<T> d;
};
}
